Rails.application.routes.draw do
  devise_for :users
  root to: "home#index", as: 'home'

  #unless Rails.application.config.consider_all_requests_local
  	get '*path', to: 'errors#render_routing_error', via: :all
  #end 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
