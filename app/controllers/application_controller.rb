class ApplicationController < ActionController::Base
  protect_from_forgery

  #unless Rails.application.config.consider_all_requests_local
	rescue_from ActionController::RoutingError, with: :render_routing_error
  #end 
  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

  # check if login is globally required to access the application
  def check_if_login_required
    # no check needed if user is already logged in
    return true if User.current.logged?
    require_login if Setting.login_required?
  end
  
end
