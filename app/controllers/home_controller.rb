class HomeController < ApplicationController
	
  def index
    @newest_projects = Project.visible.newest.take(3)
    @newest_users = User.newest.take(3)
    @news = News.latest(count: 3)
    @announcement = Announcement.active_and_current

    @homescreen = OpenProject::Static::Homescreen
  end

  def robots
    @projects = Project.active.public_projects
  end
end
